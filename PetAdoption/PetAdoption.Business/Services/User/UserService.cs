﻿using PetAdoption.Common.Domain;
using PetAdoption.DataAccess;
using PetAdoption.DataAccess.Repositories.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly DataContext _datacontext;
        public UserService(IUserRepository userRepository, DataContext datacontext)
        {
            _datacontext=datacontext;
            _userRepository=userRepository;
        }
        public AppUser GetUserById(Guid id)
        {
            return _userRepository.GetUserById(id);
        }

        public AppUser GetUserByUserName(string userName)
        {
            return _userRepository.GetUserByUserName(userName);
        }

        public AppUser GetUserByUsernameWithMessagesSent(string username)
        {
            return _userRepository.GetUserByUsernameWithMessagesSent(username);
        }

        public void UpdateUser(AppUser user)
        {
             _userRepository.UpdateUser(user);
            _datacontext.SaveChanges();

        }
    }
}
