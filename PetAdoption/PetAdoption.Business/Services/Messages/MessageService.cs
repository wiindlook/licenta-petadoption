﻿using PetAdoption.Common.Domain;
using PetAdoption.Common.Helper;
using PetAdoption.DataAccess.Repositories.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.Messages
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;
        
        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }
        public void AddMessage(Message message)
        {
            _messageRepository.AddMessage(message);
        }

        public void DeleteMessage(Message message)
        {
            _messageRepository.DeleteMessage(message);
        }

        public Message GetMessage(Guid id)
        {
          return  _messageRepository.GetMessage(id);
        }

        public IQueryable<Message> GetMessagesForUser(MessageParams messageParams)
        {
            return _messageRepository.GetMessagesForUser(messageParams);
        }

        public IEnumerable<Message> GetMessageThread(string currentUsername, string recipientUsername)
        {
            return _messageRepository.GetMessageThread(currentUsername, recipientUsername);
        }

        public IQueryable<Message> GetMessagesSentByUser(string username)
        {
            return _messageRepository.GetMessagesSentByUser(username);
        }
    }
}
