﻿using PetAdoption.Common.Domain;
using PetAdoption.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.Messages
{
    public interface IMessageService
    {
        void AddMessage(Message message);

        void DeleteMessage(Message message);

        Message GetMessage(Guid id);

        IQueryable<Message> GetMessagesForUser(MessageParams messageParams);

        IEnumerable<Message> GetMessageThread(string currentUsername, string recipientUsername);

        IQueryable<Message> GetMessagesSentByUser(string username);
    }
}
