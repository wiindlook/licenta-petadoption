﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.Advertisment
{
    
    public interface IAdvertService
    {
        IQueryable<Advert> GetAds();

        Advert GetAdvertById(Guid id);

        IQueryable<Advert> GetActiveAds();

        IQueryable<Advert> GetUnverifiedAds();

        List<Advert> GetAdverdByUserId(Guid userId);

        void CreateAdvert(Advert advert);

        void UpdateAdvert(Advert advert);

        void DeleteAdvertById(Guid id);

    }
}
