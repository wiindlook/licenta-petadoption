﻿using Microsoft.AspNetCore.Http;
using PetAdoption.Common.Domain;
using PetAdoption.DataAccess.Advertisment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.Advertisment
{
    public class AdvertService : IAdvertService
    {
        private readonly IAdvertRepository _advertRepository;
        public AdvertService(IAdvertRepository advertRepository)
        {
            _advertRepository = advertRepository;
        }

        public IQueryable<Advert> GetAds()
        {
            return _advertRepository.GetAds();
        }


        public Advert GetAdvertById(Guid id)
        {
            return _advertRepository.GetAdvertById(id);
        }

        public void CreateAdvert(Advert advert)
        {
            _advertRepository.CreateAdvert(advert);

        }

        public void UpdateAdvert(Advert advert)
        {
            _advertRepository.UpdateAdvert(advert);
        }

        public void DeleteAdvertById(Guid id)
        {
            _advertRepository.DeleteAdvertById(id);
        }

       private Advert HandleCreation()
        {
            return new Advert();
        }

        public IQueryable<Advert> GetActiveAds()
        {
            return _advertRepository.GetActiveAds();
        }

        public IQueryable<Advert> GetUnverifiedAds()
        {
            return _advertRepository.GetUnverifiedAds();
        }

        public List<Advert> GetAdverdByUserId(Guid userId)
        {
            return _advertRepository.GetAdverdByUserId(userId);
        }
    }
}
