﻿using PetAdoption.Common.Domain;
using PetAdoption.Common.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services
{
    public interface ISearchFilterCriteriaService
    {
        IQueryable<Advert> GetAdvertismentBySearchCriteria(AdSearch adSearch);
    }
}
