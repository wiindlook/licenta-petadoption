﻿using PetAdoption.Common.Domain;
using PetAdoption.Common.Search;
using PetAdoption.DataAccess.Advertisment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.Search
{
    public class SearchFilterCriteriaService : ISearchFilterCriteriaService
    {
        private readonly IAdvertRepository _advertRepository;
        public SearchFilterCriteriaService(IAdvertRepository advertRepository)
        {
            _advertRepository = advertRepository;
        }
        public IQueryable<Advert> GetAdvertismentBySearchCriteria(AdSearch adSearch)
        {
            //if(adSearch.Animal != null && adSearch.City != null && adSearch.Race != null)
            //{
            //    return _advertRepository.GetAds().Where(x => x.Animal.Race == adSearch.Race
            //                                    && x.City == adSearch.City
            //                       && x.Animal.AnimalType == adSearch.Animal);
            //}
            //else if(adSearch.Animal == "all" && adSearch.City == "all" && adSearch.Race == "all")
            //{
            //    return _advertRepository.GetAds();
            //}
            //else
            //return _advertRepository.GetAds().Where(x=> x.Animal.Race == adSearch.Race 
            //                                    || x.City == adSearch.City 
            //                       || x.Animal.AnimalType == adSearch.Animal);
            return _advertRepository.GetAds();
        }
    }
}
