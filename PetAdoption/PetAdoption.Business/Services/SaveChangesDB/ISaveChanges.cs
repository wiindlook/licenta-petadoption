﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.SaveChangesDB
{
    public interface ISaveChanges
    {
         void SaveAllChangesInDb();
         bool Complete();
    }
}
