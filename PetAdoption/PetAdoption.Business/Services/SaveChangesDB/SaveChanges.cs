﻿using PetAdoption.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Services.SaveChangesDB
{
    public class SaveChanges : ISaveChanges
    {
        private readonly DataContext _dataContext;
        public SaveChanges(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        

        public bool Complete()
        {
            return _dataContext.SaveChanges() > 0;
        }

        public void SaveAllChangesInDb()
        {
            _dataContext.SaveChanges();
        }
    }
}
