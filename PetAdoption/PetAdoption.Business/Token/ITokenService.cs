﻿using Microsoft.AspNetCore.Identity;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Business.Token
{
    public interface ITokenService
    {
        string CreateToken(AppUser user, IList<string> roles);
    }
}
