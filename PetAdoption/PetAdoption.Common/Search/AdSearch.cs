﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Common.Search
{
    public class AdSearch
    {
        public string Race { get; set; }

        public string Animal { get; set; }

        public string City { get; set; }
    }
   
}
