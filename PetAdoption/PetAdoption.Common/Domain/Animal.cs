﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Common.Domain
{
   public class Animal
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<Race> Races { get; set; }

    }
}
