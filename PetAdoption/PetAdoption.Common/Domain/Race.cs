﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PetAdoption.Common.Domain
{
    public class Race
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public virtual Animal Animal { get; set; }

        [ForeignKey("Animal")]
        public Guid Animal_Id { get; set; }


    }
}