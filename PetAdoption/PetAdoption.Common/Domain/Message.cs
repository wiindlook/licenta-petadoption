﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Common.Domain
{
    public class Message
    {
        public Guid Id { get; set; }
        [ForeignKey("Sender")]
        public Guid Sender_Id { get; set; }
        [ForeignKey("Recipient")]
        public Guid Recipient_Id { get; set; }
        public string SenderUsername { get; set; }

        public string RecipientUsername { get; set; }

        public string Content { get; set; }
        public DateTime? DateRead { get; set; }
        public DateTime MessageSent { get; set; } = DateTime.UtcNow;
        public bool SenderDeleted { get; set; }
        public bool RecipientDeleted { get; set; }
        public virtual AppUser Sender { get; set; }
        public virtual AppUser Recipient { get; set; }
    }
}
