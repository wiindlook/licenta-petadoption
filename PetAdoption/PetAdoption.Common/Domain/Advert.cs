﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Common.Domain
{
   public class Advert : BaseEntityDate
    {
        public Guid Id { get; set; }

        [ForeignKey("User")]
        public Guid User_Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Contact { get;set; }

        public string AnimalRace { get; set; } 

        public string City { get; set; } //string  sau enum

        public string PhoneNumber { get; set; }

        public string CreatedBy { get; set; }

        public bool IsPromoted { get; set; }

        public string Animal { get; set; }

        public string? AdStatusComment { get; set; }

        public bool IsVerfied { get; set; } = false;

        public bool IsActive { get; set; } = true;

        public int Views { get; set; } = 0;

        public int PhoneNumberViews { get; set; } = 0;

        public virtual List<Photo>? Photos { get; set; } = new List<Photo> { };

        public virtual AppUser User { get; set; }
    }
}
