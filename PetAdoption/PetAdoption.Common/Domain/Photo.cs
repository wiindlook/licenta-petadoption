﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.Common.Domain
{
    public class Photo
    {
        public Guid Id { get; set; }

        public string Url { get; set; }

        public string PublicId { get; set; }

        [ForeignKey("Advert")]
        public Guid Advert_Id { get; set; }

        public virtual Advert Advert { get; set; }


    }
}
