﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace PetAdoption.Common.Domain
{
    public class AppUser : IdentityUser<Guid>
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Gender { get; set; }
        public string? Description { get; set; }
        public string? City { get; set; }

        public string? Country { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime RegisteredIn { get; set; } = DateTime.Now;
        public DateTime LastActive { get; set; } = DateTime.Now;

        public int PictogramId { get; set; } = 1;
        public virtual ICollection<Advert> Advertisments {get;set;}

        public virtual ICollection<Message> MessagesSent { get; set; }
        public virtual ICollection<Message> MessagesReceived { get; set; }

    }
}
