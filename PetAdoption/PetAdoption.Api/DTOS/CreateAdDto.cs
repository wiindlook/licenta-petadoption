﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PetAdoption.Api.DTOS
{
    public class CreateAdDto
    { 
        [Required]
        public string Animal { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string City { get; set; } //string  sau enum

        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Contact { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string AnimalRace { get; set; }

        public string? CreatedBy { get; set; }
       
        public List<IFormFile>? PhotosToAdd { get; set; }


    }
}
