﻿namespace PetAdoption.Api.DTOS
{
    public class UserSenderDto
    {
        public string Username { get; set; }
        public int PictogramId { get; set; }
        public string Status { get; set; }

        public int UnreadMessages { get; set; }
    }
}
