﻿namespace PetAdoption.Api.DTOS
{
    public class MemberDto
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Gender { get; set; }
        public string? Description { get; set; }
        public string? City { get; set; }

        public string? PhoneNumber { get; set; }
        public string Email { get; set; }
        public string? Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime RegisteredIn { get; set; } 
        public DateTime LastActive { get; set; }

        public int PictogramId { get; set; }
        public virtual ICollection<AdvertDto> Advertisments { get; set; }
    }
}
