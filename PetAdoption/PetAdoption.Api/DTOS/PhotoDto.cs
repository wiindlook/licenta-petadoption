﻿namespace PetAdoption.Api.DTOS
{
    public class PhotoDto
    {
        public Guid Advert_Id { get; set; }
        public string Url { get; set; }

    }
}
