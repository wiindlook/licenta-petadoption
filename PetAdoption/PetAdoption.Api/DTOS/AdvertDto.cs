﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetAdoption.Api.DTOS
{
    public class AdvertDto
    {
        public Guid Id { get; set; }

        public Guid User_Id { get; set; }

        [Required]
        public string Animal { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 10)]
        public string Title { get; set; }
        [Required]
        [StringLength(2000, MinimumLength = 30)]
        public string Description { get; set; }

        [Required]
        public string City { get; set; } //string  sau enum
        [Required]
        [DataType(DataType.PhoneNumber)]
        [StringLength(10,MinimumLength =10)]
        public string PhoneNumber { get; set; }

        [Required]
        public string Contact { get; set; }

        public string AnimalRace { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime LastModifiedAt { get; set; }

        public string? CreatedBy { get; set; }

        public bool IsPromoted { get; set; }

        public bool IsVerified { get; set; }

        public bool IsActive { get; set; }

        public string? AdStatusComment { get; set; }

        public int? Views { get; set; }
        public int PhoneNumberViews { get; set; }

        public  ICollection<PhotoDto>? Photos { get; set; } 

    }
}
