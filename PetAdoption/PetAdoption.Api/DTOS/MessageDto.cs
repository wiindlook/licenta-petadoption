﻿namespace PetAdoption.Api.DTOS
{
    public class MessageDto
    {
        public Guid Id { get; set; }
        public Guid Sender_Id { get; set; }
        public string SenderUsername { get; set; }
        public string SenderPictogramId { get; set; }
        public Guid Recipient_Id { get; set; }
        public string RecipientUsername { get; set; }
        public string RecipientPictogramId { get; set; }
        public string Content { get; set; }
        public DateTime? DateRead { get; set; }
        public DateTime MessageSent { get; set; }
    }
}
