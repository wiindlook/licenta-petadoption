﻿namespace PetAdoption.Api.DTOS
{
    public class CreateMessageDto
    {

        public string RecipientUsername { get; set; }
        public string Content { get; set; }
    }
}
