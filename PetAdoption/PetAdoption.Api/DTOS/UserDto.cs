﻿namespace PetAdoption.Api.DTOS
{
    public class UserDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public int PictogramId { get; set; }

    }
}
