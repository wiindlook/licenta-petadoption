﻿using AutoMapper;
using PetAdoption.Api.DTOS;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetAdoption.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Advert, AdvertDto>();
            CreateMap<AdvertDto, Advert>();

            CreateMap<Photo, PhotoDto>();
            CreateMap<PhotoDto, Photo>();

            CreateMap<AppUser, MemberDto>();
            CreateMap<MemberDto, AppUser>();

            CreateMap<AppUser, RegisterDto>();
            CreateMap<RegisterDto, AppUser>();

            CreateMap<CreateAdDto, Advert>().ForMember(x => x.Photos, opt => opt.Ignore());

            CreateMap<Message, MessageDto>()
              .ForMember(dest => dest.SenderPictogramId, opt => opt.MapFrom(src =>
                  src.Sender.PictogramId))
              .ForMember(dest => dest.RecipientPictogramId, opt => opt.MapFrom(src =>
                  src.Recipient.PictogramId));
            CreateMap<MessageDto, Message>();

        }
    }
}
