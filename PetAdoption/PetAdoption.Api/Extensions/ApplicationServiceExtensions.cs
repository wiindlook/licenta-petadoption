﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PetAdoption.Api.Mapping;
using PetAdoption.Api.SignalR;
using PetAdoption.Business.Services;
using PetAdoption.Business.Services.Advertisment;
using PetAdoption.Business.Services.Messages;
using PetAdoption.Business.Services.Picture;
using PetAdoption.Business.Services.SaveChangesDB;
using PetAdoption.Business.Services.Search;
using PetAdoption.Business.Services.User;
using PetAdoption.Business.Token;
using PetAdoption.Common.Cloudinary;
using PetAdoption.DataAccess;
using PetAdoption.DataAccess.Advertisment;
using PetAdoption.DataAccess.Repositories.Messages;
using PetAdoption.DataAccess.Repositories.Picture;
using PetAdoption.DataAccess.Repositories.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetAdoption.Api.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddSingleton<PresenceTracker>();
            services.AddScoped<ITokenService, TokenService>();
            services.Configure<CloudinarySettings>(config.GetSection("CloudinarySettings"));
            services.AddDbContext<DataContext>(options => options.UseNpgsql(config.GetConnectionString("DefaultConnection")));
            services.AddAutoMapper(typeof(MappingProfile).Assembly);

            services.AddScoped<IAdvertRepository, AdvertRepository>();
            services.AddScoped<IAdvertService, AdvertService>();

            services.AddScoped<IPhotoRepository, PhotoRepository>();
            services.AddScoped<IPhotoService, PhotoService>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<ISearchFilterCriteriaService, SearchFilterCriteriaService>();

            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<IMessageService, MessageService>();

            services.AddScoped<ISaveChanges, SaveChanges>();

            return services;
        }
    }
}
