﻿using PetAdoption.Api.DTOS;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetAdoption.Api.ExtensionMethods
{
    public static class FunctionallityExtensionMethods
    {
        public static Advert ToDomain(this AdvertDto advertDto) => new Advert
        {
            Id = advertDto.Id,
            Animal = advertDto.Animal,
            Photos = (List<Photo>)advertDto.Photos,
            City = advertDto.City,
            CreatedAt = advertDto.CreatedAt,
            CreatedBy = advertDto.CreatedBy,
            Description = advertDto.Description,
            IsPromoted = advertDto.IsPromoted,
            LastModifiedAt = advertDto.LastModifiedAt,
            PhoneNumber = advertDto.PhoneNumber,
            Title = advertDto.Title,
            User_Id = advertDto.User_Id
        };
    }
}
