﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PetAdoption.Api.DTOS;
using PetAdoption.Api.Extensions;
using PetAdoption.Api.Helper;
using PetAdoption.Business.Services.Messages;
using PetAdoption.Business.Services.User;
using PetAdoption.Common.Domain;
using PetAdoption.Common.Helper;
using PetAdoption.DataAccess.Repositories.User;

namespace PetAdoption.Api.Controllers
{
    public class MessagesController : BaseApiController
    {
        private readonly IMessageService _messageService;
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public MessagesController(IMessageService messageService,
            IUserRepository userRepository, IUserService userService,
            IMapper mapper)
        {
            _messageService = messageService;
            _userRepository = userRepository;
            _userService = userService;
            _mapper = mapper;

        }

        
        [HttpPost("/api/messages/sendmessage")]
        public IActionResult CreateMessage([FromBody]CreateMessageDto createMessageDto)
        {
            var username = User.GetUsername();
            if (username == createMessageDto.RecipientUsername.ToLower())
                return BadRequest("You cannot send messages to yourself");

            var sender =  _userRepository.GetUserByUserName(username);
            var recipient =  _userRepository.GetUserByUserName(createMessageDto.RecipientUsername);

            if (recipient == null) return NotFound();

            var message = new Message
            {
                Sender = sender,
                Recipient = recipient,
                SenderUsername = sender.UserName,
                RecipientUsername = recipient.UserName,
                Content = createMessageDto.Content,

            };

            _messageService.AddMessage(message);

            return Ok(_mapper.Map<MessageDto>(message));
        }

        [HttpGet("/api/messages/getmessageforuser")]
        public IActionResult GetMessagesForUser([FromQuery]MessageParams messageParams)
        {
            messageParams.Username = User.GetUsername();

            var messages = _messageService.GetMessagesForUser(messageParams);

            var pagedMessages = PagedList<Message>.Create(messages, messageParams.PageNumber, messageParams.PageSize);

            Response.AddPaginationHeader(pagedMessages.CurrentPage, pagedMessages.PageSize,
                pagedMessages.TotalCount, pagedMessages.TotalPages);

            return Ok(_mapper.Map<List<MessageDto>>(pagedMessages.ToList()));
        }

        [HttpGet("/api/messages/getmessagethread/{username}")]
        public async Task<ActionResult<IEnumerable<MessageDto>>> GetMessageThread(string username)
        {
            var currentUsername = User.GetUsername();
            var messages = _messageService.GetMessageThread(currentUsername, username);
            var messagesDto = _mapper.Map<IEnumerable<MessageDto>>(messages);

            return Ok(messagesDto);
        }

        [HttpGet("/api/messages/getsendersforuser")]
        public IActionResult GetSendersForUser()
        {
            var currentUsername = User.GetUsername();
            var senders = AddSenders(currentUsername);
            return Ok(senders);

        }

        private List<UserSenderDto> AddSenders(string username)
        {
            List<UserSenderDto> sendersList = new List<UserSenderDto>();
            var sendersAndRecipients = _messageService.GetMessagesSentByUser(username);
            var senders = sendersAndRecipients.Select(x => x.Sender).Distinct();
            var recipients = sendersAndRecipients.Select(x => x.Recipient).Distinct();
            var allUsers = senders.Concat(recipients).Distinct().ToList();
            
            foreach(var recipient in allUsers)
            {
                if (recipient.UserName != username)
                {
                    var senderDto = new UserSenderDto { PictogramId = recipient.PictogramId,
                        Username = recipient.UserName, 
                        Status = "Online" ,
                        UnreadMessages= SetUnreadMessages (sendersAndRecipients, recipient.UserName)};

                    sendersList.Add(senderDto);
                }
            }

            return sendersList;
        }

        private int SetUnreadMessages(IQueryable<Message> messages, string recipientUsername)
        {
            int count = 0;
            var fileteredMessages = messages.Where(x => x.RecipientUsername == User.GetUsername() && x.SenderUsername == recipientUsername);
            foreach (var message in fileteredMessages)
            {
                if (message.DateRead == null) count++;
            }
            return count;
        }
    }
}
