﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PetAdoption.Api.DTOS;
using PetAdoption.Api.Extensions;
using PetAdoption.Business.Services.SaveChangesDB;
using PetAdoption.Business.Services.User;
using PetAdoption.Common.Domain;

namespace PetAdoption.Api.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ISaveChanges _saveChanges;
        public UserController(IUserService userService, IMapper mapper, ISaveChanges saveChanges)
        {
            _userService = userService;
            _mapper= mapper;
            _saveChanges= saveChanges;
        }

        [HttpGet("/api/users/getuserbyid/{id}")]
        public IActionResult GetUserById(Guid id)
        {
           var user = _userService.GetUserById(id);
           var result = _mapper.Map<MemberDto>(user);
            return Ok(result);
           
        }

        [HttpGet("/api/users/getuserbyusername/{username}")]
        public IActionResult GetUserByUserName(string username)
        {
            var user = _userService.GetUserByUserName(username);
            var result = _mapper.Map<MemberDto>(user);
            return Ok(result);
        }

        [HttpPut("/api/users/updateuser")]
        public IActionResult UpdateUser(MemberDto memberDto)
        {
            var user = _userService.GetUserByUserName(User.GetUsername());
            var userToUpdate = _mapper.Map(memberDto, user);
            _userService.UpdateUser(userToUpdate);
            return Ok();
            //if (_saveChanges.Complete()) return NoContent();

            //return BadRequest("Failed to update user");
        }

        [HttpPut("/api/users/updateuserpictogram/{id}")]
        public IActionResult UpdateUserPictogram(int id)
        {
            var username = User.GetUsername();
            var user = _userService.GetUserByUserName(username);
            user.PictogramId=id;
            _userService.UpdateUser(user);
            return Ok(user);
        }

        private AppUser UpdateAppUser(MemberDto memberDto)
        {
            return new AppUser
            {
                UserName = memberDto.Username,
                City=memberDto.City,
                Country=memberDto.Country,
                DateOfBirth=memberDto.DateOfBirth,
                Email=memberDto.Email,
                Description=memberDto.Description,
                FirstName=memberDto.FirstName,
                LastName=memberDto.LastName,
                Gender=memberDto.Gender,
                PhoneNumber =memberDto.PhoneNumber,
                PictogramId=memberDto.PictogramId

            };
        }
    }
}
