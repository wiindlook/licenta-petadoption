﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PetAdoption.Api.DTOS;
using PetAdoption.Business.Services.Advertisment;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetAdoption.Api.ExtensionMethods;
using Microsoft.AspNetCore.Authorization;
using PetAdoption.Common.Search;
using PetAdoption.Business.Services;
using PetAdoption.Business.Services.Picture;
using PetAdoption.Business.Services.User;
using PetAdoption.Api.Extensions;
using PetAdoption.Api.Helper;

namespace PetAdoption.Api.Controllers
{
    public class AdvertController : BaseApiController
    {
        private readonly IAdvertService _advertService;
        private readonly IMapper _mapper;
        private readonly ISearchFilterCriteriaService _searchFilterCriteriaService;
        private readonly IPhotoService _photoService;
        private readonly IUserService _userService;
        public AdvertController(IAdvertService advertService, IMapper mapper,
            ISearchFilterCriteriaService searchFilterCriteriaService,
            IPhotoService photoService, IUserService userService)
        {
            _advertService = advertService;
            _mapper = mapper;
            _searchFilterCriteriaService = searchFilterCriteriaService;
            _photoService = photoService;
            _userService = userService;
        }

        //[Authorize(Roles = "rolulfrumos")]
        [HttpGet("/api/getads")]
        public IActionResult GetAdverts()
        {
            var ads = _advertService.GetAds().ToList();
            var adsDto = _mapper.Map<List<AdvertDto>>(ads);
            return Ok(adsDto);
        }

        [HttpGet("/api/getads2")]
        public IActionResult GetAdverts2([FromQuery]PaginationParams paginationParams)
        {
            var ads = _advertService.GetAds();
            var pagedAds = PagedList<Advert>.Create(ads, paginationParams.PageNumber, paginationParams.PageSize);
            Response.AddPaginationHeader(pagedAds.CurrentPage, pagedAds.PageSize, pagedAds.TotalCount, pagedAds.TotalPages);

            var adsDto = _mapper.Map<List<AdvertDto>>(pagedAds.ToList());


            return Ok(adsDto);
        }

        [HttpGet("/api/getadbyid/{id}")]
        public IActionResult GetAdvertById(Guid id)
        {
            var ad = _advertService.GetAdvertById(id);
            AddView(ad);
            var adDto = _mapper.Map<AdvertDto>(ad);
            return Ok(adDto);
        }

        [HttpGet("/api/ads/getunverifiedads")]
        public IActionResult GetUnverifiedAds()
        {
            var ads = _advertService.GetUnverifiedAds();
            var adsDto = _mapper.Map<List<AdvertDto>>(ads);
            return Ok(adsDto);
        }

        [HttpGet("/api/getadsbyuser")]
        public IActionResult GetAdsByUser()
        {
            var username = User.GetUsername();
            var user = _userService.GetUserByUserName(username);
            var ads = _advertService.GetAdverdByUserId(user.Id);
            var adsDto = _mapper.Map<List<AdvertDto>>(ads);

            return Ok(adsDto);
        }

        [HttpPut("/api/ads/setverifiedstatus")]
        public IActionResult SetVerifiedStatus(AdvertDto advertDto)
        {
            var adToUpdate = _mapper.Map<Advert>(advertDto);
            _advertService.UpdateAdvert(adToUpdate);
            return Ok();
        }

        [HttpPost("/api/searchforads/")]
        public  IActionResult SeachForAds([FromBody]AdSearch adSearch)
        {
            var result = _searchFilterCriteriaService.GetAdvertismentBySearchCriteria(adSearch).ToList();

            //if (result.Any())
                
            //else
            //    return NotFound("No ads found");
            return Ok(new
            {
                merge="a mers"
            });
        }

        [HttpPost("/api/createadvert")]
        public  async Task<IActionResult> CreateAdvert([FromForm]CreateAdDto createAdDto)
        {
            var user = _userService.GetUserByUserName(User.GetUsername());
            var advertToCreate = _mapper.Map<Advert>(createAdDto);
            advertToCreate.User_Id = user.Id;
            advertToCreate.CreatedBy = user.UserName;
            await AddPhotos(createAdDto.PhotosToAdd, advertToCreate);

            _advertService.CreateAdvert(advertToCreate);
 

            return Ok();
        }

        [HttpPost("/api/test")]
        public  IActionResult Test([FromForm]CreateAdDto test)
        {

            var test2 = test;

            return Ok();
        }

        private async Task AddPhotos(ICollection<IFormFile> files, Advert advert)
        {
            if (files.Any())
            {
                foreach (var file in files)
                {
                    var result = await _photoService.AddPhotoAsync(file);

                    if (result.Error != null) return;

                    var photo = new Photo
                    {
                        Url = result.SecureUrl.AbsoluteUri,
                        PublicId = result.PublicId
                    };

                    advert.Photos.Add(photo);
                }
            }
            else return;

        }

        private void AddView(Advert advert)
        {
            advert.Views++;
            _advertService.UpdateAdvert(advert);
        }
        
        [HttpPut("/api/updateadvert")]
        public IActionResult UpdateAdvert([FromBody]AdvertDto advertDto)
        {
            _advertService.UpdateAdvert(advertDto.ToDomain());
            return Ok();
           
        }
        
        [HttpDelete("/api/deleteadvertbyid{id}")]
        public IActionResult DeleteAdvertById(Guid id)
        {
            _advertService.DeleteAdvertById(id);
            return Ok();
        }
    }
}
