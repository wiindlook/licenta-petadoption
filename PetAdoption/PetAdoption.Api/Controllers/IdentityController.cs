﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PetAdoption.Api.DTOS;
using PetAdoption.Business.Token;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetAdoption.Api.Controllers
{
    public class IdentityController:BaseApiController
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ITokenService _tokenService;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;

        public IdentityController(
            UserManager<AppUser> userManager, 
            SignInManager<AppUser> signInManager, 
            ITokenService tokenService, 
            RoleManager<IdentityRole<Guid>> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _roleManager = roleManager;
        }

        [HttpPost("/api/account/registeruser")]
        public async Task<IActionResult> RegisterUser(RegisterDto registerDto)
        {

            if (!(await _roleManager.RoleExistsAsync("User")))
            {
                await _roleManager.CreateAsync(new IdentityRole<Guid>("User"));
            }

             
            var userToCreate = new AppUser
            {
                UserName = registerDto.Username,
                Email = registerDto.Email,
                FirstName=registerDto.FirstName,
                LastName=registerDto.LastName,               
            };


            var result =await _userManager.CreateAsync(userToCreate, registerDto.Password);

            if (result.Succeeded)
            {
                var userFromDb = await _userManager.FindByNameAsync(registerDto.Username);
                await _userManager.AddToRoleAsync(userFromDb, "User");
                // de mutat in serviciu
                var roles = await _userManager.GetRolesAsync(userFromDb);

                return Ok(new UserDto
                {
                    Username = userFromDb.UserName,
                    Token = _tokenService.CreateToken(userFromDb, roles),
                    PictogramId = userFromDb.PictogramId
                }
                ) ;
            }
            return BadRequest(result.Errors);
        }

        [HttpPost("/api/account/loginuser")]
        public async Task<IActionResult> LoginUser(LoginDto loginDto)
        {
            if (ValidateLoginDto(loginDto))
            {
                return BadRequest("Username or Password cannot be empty");
            }

            var userFromDb = await _userManager.FindByNameAsync(loginDto.Username);

            if (userFromDb == null)
                return Unauthorized("Username does not exist!");

            var result = await _signInManager.CheckPasswordSignInAsync(userFromDb, loginDto.Password, false);

            if (!result.Succeeded)
                return Unauthorized("The password is incorrect");

            var roles = await _userManager.GetRolesAsync(userFromDb);
            return Ok(new UserDto
            {
                Username = userFromDb.UserName,
                Token = _tokenService.CreateToken(userFromDb, roles),
                PictogramId= userFromDb.PictogramId
                
            }) ;
            
        }

        private bool ValidateLoginDto(LoginDto loginDto)
        {
            if (string.IsNullOrEmpty(loginDto.Password) || string.IsNullOrEmpty(loginDto.Username))
                return true;

            return false;
        }
    }
}
