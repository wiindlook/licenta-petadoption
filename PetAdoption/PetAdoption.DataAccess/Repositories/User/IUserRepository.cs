﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Repositories.User
{
    public interface IUserRepository
    {
        AppUser GetUserById(Guid id);
        AppUser GetUserByUserName(string userName);

        AppUser GetUserByUsernameWithMessagesSent(string username);
        void UpdateUser(AppUser user);


    }
}
