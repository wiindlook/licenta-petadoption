﻿using Microsoft.EntityFrameworkCore;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Repositories.User
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _datacontext;
        public UserRepository(DataContext datacontext)
        {
            _datacontext = datacontext;
        }
        public AppUser GetUserById(Guid id)
        {
            return _datacontext.Users.FirstOrDefault(x => x.Id == id);
        }

        public AppUser GetUserByUserName(string userName)
        {
           return _datacontext.Users.SingleOrDefault(x => x.UserName== userName);
        }

        public AppUser GetUserByUsernameWithMessagesSent(string username)
        {
            return _datacontext.Users.Include(m => m.MessagesSent).SingleOrDefault(x => x.UserName == username);
        }

        public void UpdateUser(AppUser user)
        {
            _datacontext.Users.Update(user);
        }
    }
}
