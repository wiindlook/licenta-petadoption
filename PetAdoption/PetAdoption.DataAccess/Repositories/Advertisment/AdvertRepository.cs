﻿using Microsoft.EntityFrameworkCore;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Advertisment
{
    public class AdvertRepository : IAdvertRepository
    {
        private readonly DataContext _dataContext;
        public AdvertRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQueryable<Advert> GetAds()
        {
            return _dataContext.Ads.Include(p => p.Photos);
        }

        public IQueryable<Advert> GetActiveAds()
        {
            return _dataContext.Ads.Include(p => p.Photos).Where(x => x.IsActive == true && x.IsVerfied == true);
        }

        public Advert GetAdvertById(Guid id)
        {
            return _dataContext.Ads.Include(p => p.Photos).SingleOrDefault(x=> x.Id == id);
        }

        public List<Advert> GetAdverdByUserId(Guid userId)
        {
            return _dataContext.Ads.Include(p => p.Photos).Where(x => x.User_Id == userId).ToList();
        }

        public void CreateAdvert(Advert advert)
        {
            _dataContext.Ads.Add(advert);
            _dataContext.SaveChanges();
        }


        public void UpdateAdvert(Advert advert)
        {
            _dataContext.Ads.Update(advert);
            _dataContext.SaveChanges();
        }

        public void DeleteAdvertById(Guid id)
        {
            var ad = GetAdvertById(id);
            _dataContext.Ads.Remove(ad);
            _dataContext.SaveChanges();
        }

        public IQueryable<Advert> GetUnverifiedAds()
        {
            return _dataContext.Ads.Include(p => p.Photos).Where(x => x.IsVerfied == false);
        }
    }
}
