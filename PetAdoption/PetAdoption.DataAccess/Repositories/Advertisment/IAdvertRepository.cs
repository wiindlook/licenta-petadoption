﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Advertisment
{
    public interface IAdvertRepository
    {
        IQueryable<Advert> GetAds();

        List<Advert> GetAdverdByUserId(Guid userId);
        Advert GetAdvertById(Guid id);

        IQueryable<Advert> GetActiveAds();

        IQueryable<Advert> GetUnverifiedAds();

        void CreateAdvert(Advert advert);

        void UpdateAdvert(Advert advert);

        void DeleteAdvertById(Guid id);

    }
}
