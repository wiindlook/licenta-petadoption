﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Repositories.Picture
{
    public interface IPhotoRepository
    {
        IQueryable<Photo> GetAllPhotos();
        Photo GetPhotoById(Guid id);
        void Add(Photo photo);

       // Photo GetPhotoByUserId(Guid userId);

        IQueryable<Photo> GetPhotosByAdId(Guid advertId);

        void RemovePhotoById(Guid id);

        void RemovePhotosRange(List<Guid> photoIds);
    }
}
