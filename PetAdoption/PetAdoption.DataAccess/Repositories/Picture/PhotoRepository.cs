﻿using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Repositories.Picture
{
    public class PhotoRepository : IPhotoRepository
    {
        private readonly DataContext _dataContext;
        public PhotoRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IQueryable<Photo> GetAllPhotos()
        {
            return _dataContext.Photos;
        }

        public Photo GetPhotoById(Guid id)
        {
           return _dataContext.Photos.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Photo> GetPhotosByAdId(Guid advertId)
        {
            return _dataContext.Photos.Where(x => x.Advert_Id==advertId);
        }

        //public Photo GetPhotoByUserId(Guid userId)
        //{
        //    return _dataContext.Photos.FirstOrDefault(x => x.)
        //}
        public void Add(Photo photo)
        {
            _dataContext.Photos.Add(photo);
        }


        public void RemovePhotoById(Guid id)
        {
            var photo = GetPhotoById(id);
            _dataContext.Photos.Remove(photo);
        }


        public void RemovePhotosRange(List<Guid> photoIds)
        {
            var photosToDelete = GetAllPhotos().Where(x => photoIds.Contains(x.Id));
            _dataContext.Photos.RemoveRange(photosToDelete);
        }


    }
}
