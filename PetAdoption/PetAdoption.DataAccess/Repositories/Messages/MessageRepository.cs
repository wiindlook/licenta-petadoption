﻿using Microsoft.EntityFrameworkCore;
using PetAdoption.Common.Domain;
using PetAdoption.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Repositories.Messages
{
    public class MessageRepository : IMessageRepository
    {
        private readonly DataContext _dataContext;
        public MessageRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void AddMessage(Message message)
        {
            _dataContext.Messages.Add(message);
            _dataContext.SaveChanges();
        }

        public void DeleteMessage(Message message)
        {
            _dataContext.Remove(message);
        }

        public Message GetMessage(Guid id)
        {
            return _dataContext.Messages.Find(id);
        }

        public IQueryable<Message> GetMessagesForUser(MessageParams messageParams)
        {
            var query = _dataContext.Messages.Include(r => r.Recipient).Include(s => s.Sender)
               .OrderByDescending(m => m.MessageSent)
               .AsQueryable();

            var testquery = _dataContext.Messages
               .OrderByDescending(m => m.MessageSent).ToList();

            query = messageParams.Container switch
            {
                "Inbox" => query.Where(u => u.Recipient.UserName == messageParams.Username
                    && u.RecipientDeleted == false),
                "Outbox" => query.Where(u => u.Sender.UserName == messageParams.Username
                    && u.SenderDeleted == false),
                _ => query.Where(u => u.Recipient.UserName ==
                    messageParams.Username && u.RecipientDeleted == false && u.DateRead == null)
            };

            return query;

        }

        public IEnumerable<Message> GetMessageThread(string currentUsername, string recipientUsername)
        {
            var messages =  _dataContext.Messages
              .Include(u => u.Sender)
              .Include(u => u.Recipient)
              .Where(m => m.Recipient.UserName == currentUsername && m.RecipientDeleted == false
                      && m.Sender.UserName == recipientUsername
                      || m.Recipient.UserName == recipientUsername
                      && m.Sender.UserName == currentUsername && m.SenderDeleted == false
              )
              .OrderBy(m => m.MessageSent)
              .ToList();

            var unreadMessages = messages.Where(m => m.DateRead == null
                && m.Recipient.UserName == currentUsername).ToList();

            if (unreadMessages.Any())
            {
                foreach (var message in unreadMessages)
                {
                    message.DateRead = DateTime.Now;
                }

                _dataContext.SaveChanges();
            }

            return messages;
        }

        public IQueryable<Message> GetMessagesSentByUser(string username)
        {
           // var test = _dataContext.Messages.Where(x => x.RecipientUsername == "marcelo").ToList();
            return _dataContext.Messages.Include(r => r.Recipient).Include(s => s.Sender).Where(x => x.SenderUsername == username || x.RecipientUsername==username);
        }

        private IQueryable<Message> GetMessagesReceivedByUser(string username)
        {
            return _dataContext.Messages.Include(r => r.Sender).Where(x => x.RecipientUsername == username);
        }

        public void AddGroup(Group group)
        {
            _dataContext.Groups.Add(group);
            _dataContext.SaveChanges();
        }

        public async Task<Connection> GetConnection(string connectionId)
        {
            return await _dataContext.Connections.FindAsync(connectionId);
        }

        public async Task<Group> GetGroupForConnection(string connectionId)
        {
            return await _dataContext.Groups
                .Include(c => c.Connections)
                .Where(c => c.Connections.Any(x => x.ConnectionId == connectionId))
                .FirstOrDefaultAsync();

         
        }

        public async Task<Group> GetMessageGroup(string groupName)
        {
            return await _dataContext.Groups
                .Include(x => x.Connections)
                .FirstOrDefaultAsync(x => x.Name == groupName);
        }

        public void RemoveConnection(Connection connection)
        {
            _dataContext.Connections.Remove(connection);

            _dataContext.SaveChangesAsync();
        }

        public void AddConectionToGroup(Connection connection,Group group)
        {
            group.Connections.Add(connection);
            _dataContext.SaveChanges();
        }

    }

}
