﻿using PetAdoption.Common.Domain;
using PetAdoption.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess.Repositories.Messages
{
    public interface IMessageRepository
    {
        void AddMessage(Message message);

        void DeleteMessage(Message message);

        Message GetMessage(Guid id);

        IQueryable<Message> GetMessagesForUser(MessageParams messageParams);

        IEnumerable<Message> GetMessageThread(string currentUsername, string recipientUsername);

        IQueryable<Message> GetMessagesSentByUser(string username);

        void AddGroup(Group group);
        Task<Connection> GetConnection(string connectionId);

        Task<Group> GetGroupForConnection(string connectionId);

        Task<Group> GetMessageGroup(string groupName);

        void RemoveConnection(Connection connection);

        void AddConectionToGroup(Connection connection, Group group);
    }
}
