﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PetAdoption.Common;
using PetAdoption.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetAdoption.DataAccess
{
   public class DataContext : IdentityDbContext<AppUser, IdentityRole<Guid>, Guid>
    {

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public virtual DbSet<Advert> Ads { get; set; }

        public virtual DbSet<Photo> Photos { get; set; }

        public virtual DbSet<Animal> Animals { get; set; }

        public virtual DbSet<Race> Races { get; set; }

        public virtual DbSet<Message> Messages { get; set; }

        public DbSet<Group> Groups { get; set; }
        public DbSet<Connection> Connections { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.Entity<AppUser>()
            //   .HasMany(ur => ur.UserRoles)
            //   .WithOne(u => u.User)
            //   .HasForeignKey(ur => ur.UserId)
            //   .IsRequired();

            //builder.Entity<AppRole>()
            //    .HasMany(ur => ur.UserRoles)
            //    .WithOne(u => u.Role)
            //    .HasForeignKey(ur => ur.RoleId)
            //    .IsRequired();

            builder.Entity<Group>()
             .HasMany(x => x.Connections)
             .WithOne()
             .OnDelete(DeleteBehavior.Cascade);



            builder.Entity<Message>()
                .HasOne(u => u.Recipient)
                .WithMany(m => m.MessagesReceived)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Message>()
                .HasOne(u => u.Sender)
                .WithMany(m => m.MessagesSent)
                .OnDelete(DeleteBehavior.Restrict);

        }

        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();
            var now = DateTime.Now;

            foreach (var changedEntity in ChangeTracker.Entries())
            {
                if (changedEntity.Entity is BaseEntityDate entity)
                {
                    switch (changedEntity.State)
                    {
                        case EntityState.Added:
                            entity.CreatedAt = now;
                            entity.LastModifiedAt = now;
                            break;

                        case EntityState.Modified:
                            Entry(entity).Property(x => x.LastModifiedAt).IsModified = false;
                            entity.LastModifiedAt = now;
                            break;
                    }
                }
            }

            return base.SaveChanges();
        }
    }
}
