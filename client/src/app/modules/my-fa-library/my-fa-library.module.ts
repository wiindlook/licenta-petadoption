import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import { faSquare, faCheckSquare, faCircleUser, faFilm, faPortrait } from '@fortawesome/free-solid-svg-icons';

import {
  faStackOverflow,
  faGithub,
  faMedium,
} from '@fortawesome/free-brands-svg-icons';
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as fasStar } from '@fortawesome/free-solid-svg-icons';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class MyFaLibraryModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faSquare,
      faCheckSquare,
      faStackOverflow,
      faGithub,
      faMedium,
      fasStar,
      farStar,
      faCircleUser,
      faFilm,
      faPortrait,
    );
  }
 }
