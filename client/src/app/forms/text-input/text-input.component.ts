import { Component, Input, OnInit, Self } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent implements ControlValueAccessor {
@Input() label!:string;
@Input() type='text';
  constructor(
    @Self() public ngControl:NgControl
  ) {
    this.ngControl.valueAccessor=this;
  }
  writeValue(obj: any): void {

  }
  registerOnChange(fn: any): void {

  }
  registerOnTouched(fn: any): void {

  }

  isRequired()
  {
    //@ts-ignore
    return this.ngControl.control.errors?.required ? true : false;

  }

  isMinLength()
  {
    //@ts-ignore
    return this.ngControl.control.errors?.minlength ? true : false;
  }

  isMaxLength()
  {
    //@ts-ignore
    return this.ngControl.control.errors?.maxlength ? true : false;
  }

  isPasswordInvalid()
  {
    //@ts-ignore
    return this.ngControl.control.errors?.invalid ? true : false;
  }

  updateField(type:any)
  {
    if(type === "password")
    {
    //@ts-ignore
    this.ngControl.control?.parent?.controls['confirmPassword'].updateValueAndValidity();
    }
  }

  isEmail()
  {
       //@ts-ignore
       return this.ngControl.control.errors?.email ? true : false;
  }
}
