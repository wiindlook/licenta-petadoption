import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
requestCount=0;
isLoading = new BehaviorSubject<boolean>(false);
  constructor(private spinnerService:NgxSpinnerService) { }

  show()
  {
    this.isLoading.next(true);
    this.requestCount++;
    this.spinnerService.show(undefined,{
      bdColor:"rgba(255,255,255,0)",
       size:"large" ,
       color:"black",
        type:"ball-scale-multiple"
    });
  }

  hide()
  {
    this.isLoading.next(false);
    this.requestCount--;
    if(this.requestCount<=0)
    {
      this.requestCount=0;
      this.spinnerService.hide();
    }
  }

}
