import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, map, Observable, of } from 'rxjs';
import { getPaginatedResult, getPaginationHeaders } from '../helpers/paginationHelper';
import { AppUser } from '../models/appuser';
import { PaginationParams } from '../models/paginationParams';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl = 'https://localhost:44374';
  breeds:Array<any>=[];
  constructor(private http : HttpClient) { }

  getBreeds()
  {
    if(this.breeds.length > 0) return of(this.breeds);
    return this.http.get('https://api.thedogapi.com/v1/breeds?limit=1000&page=0').pipe(map(
      (breeds:any) => {
        this.breeds=breeds;
        return breeds;
      }
    ));
  }

  searchForAds(obj:any)
  {
    return this.http.post('https://localhost:44374/api/searchforads/',obj);
  }

  getUserByUsername(username:any)
  {
    return this.http.get<AppUser>(this.baseUrl+'/api/users/getuserbyusername/'+username);
  }

  updateUserPictogram(id:any)
  {
    return this.http.put(this.baseUrl + '/api/users/updateuserpictogram/'+id,{});
  }

  updateUser(user:any)
  {
    return this.http.put(this.baseUrl + "/api/users/updateuser", user);
  }

  createAd(model:any)
  {
    return this.http.post(this.baseUrl + "/api/createadvert", model)
  }

  getAds(pagParams: PaginationParams)
  {
    var params = getPaginationHeaders(pagParams.pageNumber,pagParams.pageSize)

    return getPaginatedResult(this.baseUrl + "/api/getads2", params,  this.http)
    return this.http.get(this.baseUrl + "/api/getads2");
  }

  getAdById(id:any)
  {
    return this.http.get(this.baseUrl + '/api/getadbyid/'+id);
  }

  getUserById(id:any)
  {
    return this.http.get(this.baseUrl + '/api/users/getuserbyid/'+ id);
  }

  getAdsByUser()
  {
    return this.http.get(this.baseUrl + '/api/getadsbyuser');
  }


}
