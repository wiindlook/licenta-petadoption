import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { getPaginatedResult, getPaginationHeaders } from '../helpers/paginationHelper';
import { User } from '../models/user';
import { take } from 'rxjs/operators';
import { Group } from '../models/group';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  baseUrl = 'https://localhost:44374'
  hubUrl = environment.hubUrl;
  private hubConnection!: HubConnection;
  private messageThreadSource = new BehaviorSubject<any[]>([]);
  messageThread$ = this.messageThreadSource.asObservable();

  constructor(private http: HttpClient) { }

  getMessages(pageNumber:any, pageSize:any, container:any) {
    let params = getPaginationHeaders(pageNumber, pageSize);
    params = params.append('Container', container);
    return getPaginatedResult<any>(this.baseUrl + '/api/messages/getmessageforuser', params, this.http);
  }

  getMessageThread(username: string) {
    return this.http.get<any[]>(this.baseUrl + '/api/messages/getmessagethread/' + username);
  }

  // sendMessage(username: string, content: string) {
  //   return this.http.post<any>(this.baseUrl + '/api/messages/sendmessage', {recipientUsername: username, content})
  // }

  deleteMessage(id: any) {
    return this.http.delete(this.baseUrl + 'messages/' + id);
  }

  getSendersForUser()
  {
    return this.http.get(this.baseUrl + '/api/messages/getsendersforuser')
  }


  createHubConnection(user: User, otherUsername: string) {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(this.hubUrl + 'message?user=' + otherUsername, {
        accessTokenFactory: () => user.token
      })
      .withAutomaticReconnect()
      .build()

    this.hubConnection.start().catch(error => console.log(error));

    this.hubConnection.on('ReceiveMessageThread', messages => {
      this.messageThreadSource.next(messages);
    })

    this.hubConnection.on('NewMessage', message => {
      this.messageThread$.pipe(take(1)).subscribe(messages => {
        this.messageThreadSource.next([...messages, message])
      })
    })

    this.hubConnection.on('UpdatedGroup', (group: Group) => {
      if (group.connections.some(x => x.username === otherUsername)) {
        this.messageThread$.pipe(take(1)).subscribe(messages => {
          messages.forEach(message => {
            if (!message.dateRead) {
              message.dateRead = new Date(Date.now())
            }
          })
          this.messageThreadSource.next([...messages]);
        })
      }
    })
  }

  stopHubConnection() {
    if (this.hubConnection) {
      this.hubConnection.stop();
    }
  }

  async sendMessage(username: string, content: string) {
    return this.hubConnection.invoke('SendMessage', {recipientUsername: username, content})
      .catch(error => console.log(error));
  }

}
