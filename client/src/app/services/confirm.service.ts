import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { ConfirmationDialogComponent } from '../confirmation/confirmation-dialog/confirmation-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ConfirmService {
bsModalRef!:BsModalRef;
  constructor(private modalService :BsModalService) { }

  confirm(title='Confirmation',
  message='Are you sure you want to do this?',
  btnOkText='Ok',
  btnCancelText='Cancel'): Observable<boolean>
  {
    const config = {
      initialState:{
        title,
        message,
        btnOkText,
        btnCancelText
      }
    }
    this.bsModalRef = this.modalService.show(ConfirmationDialogComponent, config)

    return new Observable<boolean>(this.getResult())
  }

  private getResult()
  {
    return (observer : any) => {
      const subscribtion = this.bsModalRef.onHidden!.subscribe(() => {
        observer.next(this.bsModalRef.content.result);
        observer.complete();
      });

      return {
         unsubscribe(){
           subscribtion?.unsubscribe()
         }
      }
    }
  }
}
