import { Directive, HostListener } from '@angular/core';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';

@Directive({
  selector: 'input[ngbtypeaheadpopup]'
})
export class PopupDirective {
  @HostListener('focus', ['$event.target'])
  @HostListener('click', ['$event.target'])
  onClick(t: { dispatchEvent: (arg0: Event) => void; }) { if (!this.typeahead.isPopupOpen()) t.dispatchEvent(new Event('input')) }

  constructor(private typeahead: NgbTypeahead) {
  }

}
