import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import 'bootstrap/dist/js/bootstrap.bundle';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PopupDirective } from './directives/popup.directive';
import { HomeComponent } from './pages/home/home.component';
import { CreateadvertComponent } from './pages/advert/createadvert/createadvert.component';
import { AdvertdetailComponent } from './pages/advert/createadvert/advertdetail/advertdetail.component';
import { UserprofileComponent } from './pages/user/userprofile/userprofile.component';
import { PremiuminfoComponent } from './pages/premiuminfo/premiuminfo.component';
import { SearchadsComponent } from './pages/advert/createadvert/searchads/searchads.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { RegisteruserModal } from './pages/user/modals/registeruser/registeruser.modal';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoginuserModal } from './pages/user/modals/loginuser/loginuser.modal';
import { TestErrorsComponent } from './errors/test-errors/test-errors.component';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { ServerErrorComponent } from './errors/server-error/server-error.component';
import { MyFaLibraryModule } from './modules/my-fa-library/my-fa-library.module';
import { UserphotoComponent } from './pages/user/userphoto/userphoto.component';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ConfirmationDialogComponent } from './confirmation/confirmation-dialog/confirmation-dialog.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SpinnerInterceptor } from './interceptors/spinner.interceptor';
import { TextInputComponent } from './forms/text-input/text-input.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { TextareaInputComponent } from './forms/text-input/textarea-input/textarea-input/textarea-input.component';
import { AdvertsComponent } from './pages/advert/createadvert/adverts/adverts.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { MessagesComponent } from './pages/user/userprofile/messages.component';
import { UserChatComponent } from './pages/user/userchat/user-chat.component';
import { ChatbodyComponent } from './pages/user/userchat/chatbody.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    PopupDirective,
    CreateadvertComponent,
    AdvertdetailComponent,
    UserprofileComponent,
    PremiuminfoComponent,
    SearchadsComponent,
    RegisteruserModal,
    LoginuserModal,
    TestErrorsComponent,
    NotFoundComponent,
    ServerErrorComponent,
    UserphotoComponent,
    ConfirmationDialogComponent,
    TextInputComponent,
    TextareaInputComponent,
    AdvertsComponent,
    MessagesComponent,
    UserChatComponent,
    ChatbodyComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    MyFaLibraryModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    NgxDropzoneModule,
    NgxGalleryModule,
    NgbPaginationModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    ToastrModule.forRoot({
      positionClass:'toast-bottom-right'
    }),
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: SpinnerInterceptor, multi: true}
  ],
  exports:[
    BsDropdownModule,
    ToastrModule,

  ],
  bootstrap: [AppComponent]
})
export class AppModule {

 }
