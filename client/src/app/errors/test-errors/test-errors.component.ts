import { HttpClient} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-errors',
  templateUrl: './test-errors.component.html',
  styleUrls: ['./test-errors.component.scss']
})
export class TestErrorsComponent implements OnInit {
baseUrl = 'https://localhost:44374/'
validationErrors:string[]=[];
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  get404Error(){
    this.http.get(this.baseUrl + 'not-found').subscribe({
      next : (v) => {console.log(v)},
      error : (e) =>{console.log(e)}
    })
  }

  get400Error(){
    this.http.get(this.baseUrl + 'bad-request').subscribe({
      next : (v) => {console.log(v)},
      error : (e) =>{console.log(e)}
    })
  }

  get500Error(){
    this.http.get(this.baseUrl + 'server-error').subscribe({
      next : (v) => {console.log(v)},
      error : (e) =>{console.log(e)}
    })
  }

  get401Error(){
    this.http.get(this.baseUrl + 'auth').subscribe({
      next : (v) => {console.log(v)},
      error : (e) =>{console.log(e)}
    })
  }

  get400ValidationError(){
    this.http.post(this.baseUrl + 'api/account/registeruser', {}).subscribe({
      next : (v) => {console.log(v)},
      complete: () => {console.info()},
      error : (e) =>{console.log(e)
      this.validationErrors=e}
    })
  }

}
