import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundError } from 'rxjs';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { ServerErrorComponent } from './errors/server-error/server-error.component';
import { TestErrorsComponent } from './errors/test-errors/test-errors.component';
import { AuthGuard } from './guards/auth.guard';
import { PreventUnsavedChangesGuard } from './guards/prevent-unsaved-changes.guard';
import { AdvertdetailComponent } from './pages/advert/createadvert/advertdetail/advertdetail.component';
import { AdvertsComponent } from './pages/advert/createadvert/adverts/adverts.component';
import { CreateadvertComponent } from './pages/advert/createadvert/createadvert.component';
import { SearchadsComponent } from './pages/advert/createadvert/searchads/searchads.component';
import { HomeComponent, searchModel } from './pages/home/home.component';
import { PremiuminfoComponent } from './pages/premiuminfo/premiuminfo.component';
import { UserChatComponent } from './pages/user/userchat/user-chat.component';
import { MessagesComponent } from './pages/user/userprofile/messages.component';
import { UserprofileComponent } from './pages/user/userprofile/userprofile.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'createad', component:CreateadvertComponent}, //, canActivate:[AuthGuard]
  {path:'adverts/:id', component:AdvertdetailComponent},
  {path:'adverts', component:AdvertsComponent},
  {path:'premium', component:PremiuminfoComponent, canActivate:[AuthGuard]},
  {path:'searchresult', component:SearchadsComponent},
  {path:'userprofile/:username', component:UserprofileComponent,canActivate:[AuthGuard], canDeactivate:[PreventUnsavedChangesGuard]},
  {path:'chat',component:UserChatComponent},
  {path:'errors', component:TestErrorsComponent},
  {path:'not-found',component:NotFoundComponent},
  {path:'server-error',component:ServerErrorComponent},
  {path:'**', component:HomeComponent, pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
