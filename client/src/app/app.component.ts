import { Component } from '@angular/core';
import { User } from './models/user';
import { AccountService } from './services/account.service';
import { PressenceService } from './services/pressence.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';

  constructor(private accountService: AccountService, private presenceService: PressenceService) {}

  ngOnInit() {
    this.setCurrentUser();
  }

  setCurrentUser() {
    const user: User = JSON.parse(localStorage.getItem('user') ?? null!);
    if(user)
    {
    this.accountService.setCurrentUser(user);
    this.presenceService.createHubConnection(user);
    }
}
}
