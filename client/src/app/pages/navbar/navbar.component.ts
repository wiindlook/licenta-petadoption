import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastRef, ToastrService } from 'ngx-toastr';
import { AccountService } from 'src/app/services/account.service';
import { LoginuserModal } from '../user/modals/loginuser/loginuser.modal';
import { RegisteruserModal } from '../user/modals/registeruser/registeruser.modal';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private toastr:ToastrService,
    private modalService:NgbModal,
    public accountService:AccountService,
    private router:Router) { }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe(x=>console.log(x));
  }


  openRegisterModal()
  {
   // this.showSpinner();
    const modalRef = this.modalService.open(RegisteruserModal, {size:'md'});
  }

  openLoginModal()
  {
    const modalRef =this.modalService.open(LoginuserModal);
  }

  logout()
  {
    this.accountService.logout();
      this.router.navigateByUrl('/');



  }

  getUserPhoto(pictogramId:number)
{
  return "/assets/profileicons/" + pictogramId + ".jpg"
}
}
