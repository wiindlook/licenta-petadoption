import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Advert } from 'src/app/models/advert';
import { Pagination } from 'src/app/models/pagination';
import { PaginationParams } from 'src/app/models/paginationParams';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-adverts',
  templateUrl: './adverts.component.html',
  styleUrls: ['./adverts.component.scss']
})
export class AdvertsComponent implements OnInit {

  ads:Array<Advert>=[];
  adverts:any;
  pagination!:Pagination;
  paginationParams:PaginationParams = new PaginationParams();
  isLoading:boolean=false;
  constructor(private dataService: DataService,
              private router:Router) { }

  ngOnInit(): void {
    this.loadAds();
  }

  loadAds()
  {
    this.isLoading=true;
    this.dataService.getAds(this.paginationParams).subscribe((paginatedAds:any)=>{
      this.adverts=paginatedAds.result,
      this.pagination=paginatedAds.pagination;

      console.log(this.adverts);
      console.log(this.pagination);

    });
    this.isLoading=false;
  }

  pageChanged(event:any)
  {
    this.paginationParams.pageNumber=event;
    this.loadAds();
  }

  setCreatedAt(date:any)
  {
    let currentDate = new Date();
    let adDate = new Date(date);

    let hour= adDate.getHours();
    let minutes= adDate.getMinutes();

    let day= Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(adDate.getFullYear(), adDate.getMonth(), adDate.getDate()) ) /(1000 * 60 * 60 * 24));


     if(day<1)
     return "Astazi " + hour + ":" + minutes;
     else if(day >=1)
     return adDate.getDate() + " " + this.setMonth(adDate.getMonth()) + " " + adDate.getFullYear() ;

     return;
  }

  sortAscByDate()
  {
    this.adverts.sort(function(a:any,b:any){
      //@ts-ignore
      return new Date(b.createdAt) - new Date(a.createdAt);
    });
  }

  sortDescByDate()
  {
    this.adverts.sort(function(a:any,b:any){
      //@ts-ignore
      return new Date(a.createdAt) - new Date(b.createdAt);
    });
  }

  setMonth(id:any)
  {
    let month="";
    switch(id)
    {
      case 0:
       month="Ianuarie";
       break;
       case 1:
       month="Februarie";
       break;
       case 2:
       month="Martie";
       break;
       case 3:
       month="Aprilie";
       break;
       case 4:
       month="Mai";
       break;
       case 5:
       month="Iunie";
       break;
       case 6:
       month="Iulie";
       break;
       case 7:
       month="August";
       break;
       case 8:
       month="Septembrie";
       break;
       case 9:
       month="Octombrie";
       break;
       case 10:
       month="Noiembrie";
       break;
       case 11:
       month="Decembrie";
       break;
    }

    return month;
  }

}
