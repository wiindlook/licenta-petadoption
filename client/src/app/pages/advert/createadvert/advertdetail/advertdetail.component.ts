import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from '@kolkov/ngx-gallery';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-advertdetail',
  templateUrl: './advertdetail.component.html',
  styleUrls: ['./advertdetail.component.scss']
})
export class AdvertdetailComponent implements OnInit, OnDestroy {

  galleryOptions!: NgxGalleryOptions[];
  galleryImages!: NgxGalleryImage[];

  routeSub?: Subscription ;
  advert:any;
  user:any;
  showNumber:boolean=false;

  constructor(private route: ActivatedRoute,
              private dataService: DataService) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(x=>{
      this.loadAd(x['id']);
    })

    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnailsColumns: 4,

        imageAnimation: NgxGalleryAnimation.Slide
      },
    ]

  }

  getImages()
  {
    const imgUrls=[];
    for(const photo of this.advert.photos)
    {
      imgUrls.push({
        small:photo?.url,
        medium:photo?.url,
        big:photo?.url
      })
    }

    return imgUrls;
  }

  loadAd(id:any)
  {
    this.dataService.getAdById(id).subscribe({
      next: x => {this.advert= x, console.log(this.advert),
        this.galleryImages=this.getImages(),this.loadUser(this.advert.user_Id)}

    })
  }

  getUserPhoto()
  {
  return "/assets/profileicons/" + this.user?.pictogramId + ".jpg"
  }

  show()
  {
    this.showNumber=!this.showNumber;
  }

  loadUser(id:any)
  {
    this.dataService.getUserById(id).subscribe({
      next: x => {this.user=x,console.log(this.user)}
    })
  }

  setIconBasedOnAnimal(animal:any)
  {
    if(animal.toLowerCase() === "dog")
    return "fa-solid fa-cat";

    if(animal.toLowerCase() === "dog")
    return "fa-solid fa-dog";

    return;
  }

  ngOnDestroy(): void {
    this.routeSub?.unsubscribe();
  }

}
