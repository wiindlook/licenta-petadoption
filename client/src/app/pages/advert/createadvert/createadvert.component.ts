import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { debounce, debounceTime, distinctUntilChanged, map, Observable, OperatorFunction, timer } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-createadvert',
  templateUrl: './createadvert.component.html',
  styleUrls: ['./createadvert.component.scss']
})
export class CreateadvertComponent implements OnInit {

  files: File[] = [];
  test:any=[];
  model:any={};

  createAdForm!:FormGroup;

  animal:Array<string>=["Dog", "Cat"];  

  catRaces:Array<string>=["maidaneza","birmaneza"];
  dogRaces:Array<string>=["maidanez","hasky"];
  mainRace:Array<string>=[];

  constructor(private toastr: ToastrService,
    private dataService: DataService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initializeForm();
  }


  initializeForm()
  {
    this.createAdForm=this.fb.group({
      title: ['',[Validators.required,Validators.minLength(10),Validators.maxLength(30)]],
      animal:['',[Validators.required]],
      animalRace: ['', [Validators.required]],
      description: ['', [Validators.required, Validators.minLength(30),Validators.maxLength(500)]],
      contact:['', [Validators.required] ],
      city: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required,Validators.minLength(10),Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.email]],
    })
  }

	onSelect(event: any) {
		console.log(event);
    if(event.addedFiles?.length <= 5)
    {
		this.files.push(...event.addedFiles);
    }
    else {
      this.files.push(...event.addedFiles.slice(0, 5))
      this.toastr.error("You can only upload up to 5 pictures!")
    }
	}

	onRemove(event: any) {
		console.log(event);
		this.files.splice(this.files.indexOf(event), 1);
	}

  createAd()
  {
    let formData =new FormData();

    this.model.photosToAdd=this.files;
    Object.keys(this.createAdForm.value).forEach(key => {
      formData.append(key, this.createAdForm.value[key])
    });

    for(let i =0 ;i<this.files.length;i++)
    {
      formData.append('photosToAdd',this.files[i])
    }


    this.dataService.createAd(formData).subscribe(x =>{console.log(x)})
  }
  searchForAnimal = (text$: Observable<string>):Observable<string[]> => {
    return text$.pipe(
      debounce(t => timer(t.length > 0 ? 200:0)),
      distinctUntilChanged(),
      map(term => term === '' ? this.animal
        : this.animal.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0,20))
    )};

    searchForRace: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : this.mainRace.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  setAnimalRace(item:any)
  {
    this.createAdForm.get('animal')?.setValue(item.item);
    const animal =this.createAdForm.get('animal')?.value;
    animal === "Cat"? this.mainRace=this.catRaces : this.mainRace=this.dogRaces;
    //this.httpget('api/animalraces/', animalname)
  }

  isAnimalFieldEmpty()
  {
    const test =  this.createAdForm.get('animal')?.value
    if(test)
    return false;

    return true;
  }

}


