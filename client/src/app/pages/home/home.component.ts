import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { debounce, debounceTime, delay, distinctUntilChanged, map, Observable, OperatorFunction, timer } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { DataService } from 'src/app/services/data.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private dataservice:DataService,
    public spinnerService:SpinnerService,
    public router : Router) { }

  searchModel:searchModel=new searchModel("","","");
  selectedValue:string="";
  data:Array<string>=[];
  animal:Array<string>=["Dog","Cat","Parrot"];
  city:Array<string>=["Brasov", "Ploiesti", "Piatra-Neamt"];
  data2:any=["1","23","3333"]
  test:any;
  ngOnInit(): void {
    this.addBreeds();
  }


  formatter = (searchModel: searchModel) => searchModel.animal;

  searchForRace: OperatorFunction<string, readonly string[]> = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 1 ? []
      : this.data.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )

  //toUpdateTheRestWithThis still unsure this is the best decision
  searchForAnimal = (text$: Observable<string>):Observable<string[]> => {
    return text$.pipe(
      debounce(t => timer(t.length > 0 ? 200:0)),
      distinctUntilChanged(),
      map(term => term === '' ? this.animal
        : this.animal.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0,20))
    )};

    searchForCity = (text$: Observable<string>):Observable<string[]> => {
      return text$.pipe(
        debounce(t => timer(t.length > 0 ? 200:0)),
        distinctUntilChanged(),
        map(term => term === '' ? this.city
          : this.city.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0,20))
      )};

      //to do add css class to make it look disabled
    isAnimalEmpty()
    {
      // if(this.searchModel.animal ===undefined || this.searchModel.animal == null || this.searchModel.animal.length === 0)
      // return true;

      return false;
    }

    //to do make your own api calls that can bring back animals based on type(dog,cat,etc);
    //   /api/getanimal/{type}
    addBreeds()
      {
          this.dataservice.getBreeds().subscribe((result : any)=> {
            result.forEach((dog:dog) => {
              this.data.push(dog.name);
            });;
          })
      }


    submit()
      {
        this.router.navigateByUrl("/adverts")
        this.dataservice.searchForAds(this.searchModel).subscribe(x=>{
          this.test=x;
          console.log(this.test);
        })
      }

}

export class dog{
  id:number;
  name:string;
  constructor(id:number,name:string)
  {
    this.id=id,
    this.name=name
  }
}

export class searchModel{
  animal:string;
  city:string;
  race:string;
  constructor(animal:string, city:string, race:string)
  {
    this.animal=animal;
    this.city=city;
    this.race=race;
  }
}
