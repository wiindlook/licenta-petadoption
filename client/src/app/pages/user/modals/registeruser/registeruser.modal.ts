import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.modal.html',
  styleUrls: ['./registeruser.modal.scss']
})
export class RegisteruserModal implements OnInit {
model:any={};
registerForm! : FormGroup;
validationErrors: string[] = [];

  constructor(public activeModal:NgbActiveModal,
    private accountService : AccountService,
    private toastr : ToastrService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm()
  {
    this.registerForm=this.fb.group({
      username: ['',[Validators.required,Validators.minLength(4), Validators.maxLength(15)]],
      firstName:['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password:['', [Validators.required, Validators.minLength(4)]],
      confirmPassword: ['',[Validators.required, this.validatePassword('password')]]
    })
  }

  validatePassword(password : string) : ValidatorFn //validator function
  {
    return (control: AbstractControl) =>{
      //@ts-ignore
      return control?.value === control?.parent?.controls[password].value ? null : {invalid: true}; // if check fails return validation error invalid=true;
    }
  }

  updateField()
  {
    this.registerForm.controls['confirmPassword'].updateValueAndValidity();
  }

  register()
  {
    this.accountService.register(this.registerForm.value).subscribe( {
      complete :() => this.activeModal.close(),
      error: (e) =>  {
        if(e.error)
        {
          this.validationErrors.length=0;
         e.error.forEach((er:any) => {
           this.validationErrors.push(er.description);
         });
        }else
        {
        this.validationErrors.length=0;
        this.validationErrors=e;
        }
      }
  });
}
}
