import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-loginuser',
  templateUrl: './loginuser.modal.html',
  styleUrls: ['./loginuser.modal.scss']
})
export class LoginuserModal implements OnInit {
model:any={};
  constructor(public activeModal:NgbActiveModal,
     private accountService: AccountService,
     private toastr : ToastrService) { }

  ngOnInit(): void {
  }

  login(){
    this.accountService.login(this.model).subscribe({
      complete : () => {this.activeModal.close()},
    });
  }



  logout()
  {
    this.accountService.logout();
  }
}
