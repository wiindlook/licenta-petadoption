import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { Pagination } from 'src/app/models/pagination';
import { PaginationParams } from 'src/app/models/paginationParams';
import { AccountService } from 'src/app/services/account.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(private messageService: MessageService,private accountService:AccountService) {
    this.accountService.currentUser$.subscribe(user => this.currentUser = user)
   }
  messages: any;
  pagination!: Pagination;
  pageNumber = 1;
  pageSize = 5;
  container:string="Unread";
  loading = false;
  messageType=MessageType;
  currentUser:any;

  ngOnInit(): void {
    this.loadMessages();
  }

  loadMessages(container:string='Unread') {
    this.container=container;
    this.loading = true;
    this.messageService.getMessages(this.pageNumber, this.pageSize, container).subscribe(response => {
      this.messages = response.result;
      this.pagination = response.pagination;
      this.loading = false;
    })
  }

  deleteMessage(id: string) {
    this.messageService.deleteMessage(id).subscribe(() => {
      this.messages.splice(this.messages.findIndex((m:any) => m.id === id), 1);
    })
  }

  pageChanged(event: any) {
    this.pageNumber = event.page;
    this.loadMessages();
  }

  getUserPhoto(id:any)
{
  return "/assets/profileicons/" + id + ".jpg"
}
}



export enum MessageType{
  Unread,
  Inbox,
  Outbox
}
