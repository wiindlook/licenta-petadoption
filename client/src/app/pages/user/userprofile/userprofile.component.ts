import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalBackdrop } from '@ng-bootstrap/ng-bootstrap/modal/modal-backdrop';
import { ToastrService } from 'ngx-toastr';
import { Observable, take } from 'rxjs';
import { AccountService } from 'src/app/services/account.service';
import { DataService } from 'src/app/services/data.service';
import { UserphotoComponent } from '../userphoto/userphoto.component';


@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {
  @ViewChild('editForm') editForm! :NgForm;
  active=1;

  isEdit=false;
  loggedUser:any;
  appUser?:any;
  userAds$!:Observable<any>;



  constructor(private dataService: DataService,
     private accountService:AccountService,
     private modalService:NgbModal,
     private toastr: ToastrService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user=> this.loggedUser=user);
   }

  ngOnInit(): void {
    this.loadUserDetails();
    this.addUserAds$();
    this.dataService.getAdsByUser().subscribe(x=> console.log(x));
  }

loadUserDetails()
{
  this.dataService.getUserByUsername(this.loggedUser.username).subscribe({
    next :(userFromDb)=>{this.appUser=userFromDb}
  })
}

editProfile()
{
  this.isEdit=!this.isEdit
  if(this.isEdit===false)
  {
    this.editForm.reset(this.appUser);
  }
}

save()
{
  this.dataService.updateUser(this.appUser).subscribe(() =>
     this.toastr.success('Your changes have been made.'));
     this.editForm.reset(this.appUser);
  this.isEdit=false;

}

getUserPhoto()
{
  return "/assets/profileicons/" + this.appUser?.pictogramId + ".jpg"
}

openPhotoModal()
{
  const modalRef = this.modalService.open(UserphotoComponent, {size:'lg',backdrop:'static'});
  modalRef.componentInstance.myUser=this.appUser;
}

addUserAds$()
{
  this.userAds$= this.dataService.getAdsByUser();
}

setTextForAdStatus(ad:any)
{
  if(ad.isActive === false)
  return "The ad is closed.";

  if(ad.isVerified === true && ad.isActive)
  return "The ad is open.";
  else return "The ad is not verified yet.";

}

setTab(tab:any)
{
  this.active=tab;
}

setAdDAte(date:any)
{

}

}
