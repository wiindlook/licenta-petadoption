import { Component, Input, OnInit } from '@angular/core';
import { Data } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Toast, ToastrService } from 'ngx-toastr';
import { AppUser } from 'src/app/models/appuser';
import { AccountService } from 'src/app/services/account.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-userphoto',
  templateUrl: './userphoto.component.html',
  styleUrls: ['./userphoto.component.scss']
})
export class UserphotoComponent implements OnInit {
  @Input()
  myUser:any;

  icons:Array<number>=[2,3,4,5,6];
  initalUserPictogramId:number=0;

  loggedUser:any;
  isPhotoClicked:boolean=false;
  selectedPhoto:any;
  constructor(public activeModal:NgbActiveModal,
    private accountService:AccountService,
    private dataService:DataService,
    private toastr: ToastrService) {
      this.accountService.currentUser$.subscribe({
        next: (user) => this.loggedUser=user
      });
    }

  ngOnInit(): void {
    this.initalUserPictogramId=this.myUser.pictogramId;
  }

showPhoto(id:any)
{
  return  "/assets/profileicons/" + id + ".jpg"
}
addPhotoToUser(id:any){
  this.selectedPhoto=id;
  this.myUser.pictogramId=id;
  this.isPhotoClicked=true;
}

savePhoto(){
 this.dataService.updateUserPictogram(this.myUser.pictogramId).subscribe((x:any) => x);
 this.activeModal.close();
}

cancel()
{
  if(this.myUser.pictogramId != this.initalUserPictogramId)
  this.toastr.error('You have unsaved changes');
else
  this.activeModal.close();
}
}
