import { Component, Input, OnInit } from '@angular/core';
import { AccountService } from 'src/app/services/account.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-chatbody',
  templateUrl: './chatbody.component.html',
  styleUrls: ['./chatbody.component.scss']
})
export class ChatbodyComponent implements OnInit {

  constructor(public messageService: MessageService, private accountService: AccountService) {
   this.accountService.currentUser$.subscribe(x=>  this.currentUsername= x.username)
   }

  @Input() recipientUsername:string="";
  @Input() recipientPictogramId:any;
  @Input() reloadFlag:boolean=false;
  @Input() messages:any;
  messageContent:string="";
  currentUsername:any;
  loading:boolean=false;


  ngOnInit(): void {

  }

  // sendMessage() {
  //   this.messageService.sendMessage(this.recipientUsername, this.messageContent).subscribe(message => {
  //     this.messages?.push(message);
  //     this.messageContent="";
  //     ///this.messageForm?.reset();
  //   })

    sendMessage() {
      this.loading = true;
      this.messageService.sendMessage(this.recipientUsername, this.messageContent).then(() => {
        this.messageContent="";
      }).finally(() => this.loading = false);
    }



showPhoto(id:any)
{
return  "/assets/profileicons/" + id + ".jpg"
}
}
