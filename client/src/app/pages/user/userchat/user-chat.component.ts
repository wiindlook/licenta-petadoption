import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AccountService } from 'src/app/services/account.service';
import { MessageService } from 'src/app/services/message.service';
import { PressenceService } from 'src/app/services/pressence.service';

@Component({
  selector: 'app-user-chat',
  templateUrl: './user-chat.component.html',
  styleUrls: ['./user-chat.component.scss']
})
export class UserChatComponent implements OnInit, OnDestroy{
  senders:any;
  currentUser:any;
  conversationOpened:boolean=false;
  usernameToPass:any;
  recipientPictogram:any;
  conversation:any;
  previousUser:any;

  constructor(private messageService: MessageService, public presenceService : PressenceService, private accountService: AccountService) {
    this.accountService.currentUser$.subscribe(x => this.currentUser=x);
   }


  ngOnInit(): void {
    this.getSendersForUser();
  }

  getSendersForUser()
  {
    this.messageService.getSendersForUser().subscribe(x => this.senders = x);
  }




  showPhoto(id:any)
  {
  return  "/assets/profileicons/" + id + ".jpg"
  }

  openConversation(recipientUsername :any)
  {
    this.usernameToPass=recipientUsername;

    this.messageService.createHubConnection(this.currentUser, recipientUsername);
    this.recipientPictogram=this.senders.find((x:any) => x.username == recipientUsername).pictogramId;
  }

  ngOnDestroy() {
    this.messageService.stopHubConnection();
  }
}
