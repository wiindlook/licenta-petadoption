import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserprofileComponent } from '../pages/user/userprofile/userprofile.component';
import { ConfirmService } from '../services/confirm.service';

@Injectable({
  providedIn: 'root'
})
export class PreventUnsavedChangesGuard implements CanDeactivate<unknown> {
  constructor(private confirmService :ConfirmService){}
  canDeactivate(
    component: UserprofileComponent): Observable<boolean> | boolean {
      if(component.isEdit)
      {
        return this.confirmService.confirm();
      }
    return true;
  }

}
