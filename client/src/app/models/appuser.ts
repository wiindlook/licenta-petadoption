export interface AppUser{
  id:any;
  username:string;
  firstname:string;
  lastname:string;
  gender:string;
  description:string;
  city:string;
  country:string;
  dateofbirth:Date;
  registeredIn:Date;
  lastActive:Date;
  pictogramId:number;
  advertisments:[];
}
