

 export interface Advert{
 id:any;
 user_Id:any;
 animal:string;
 title:string;
 description:string;
 city:string;
 phoneNumber:string;
 contact:string;
 createdAt:Date;
 lastModifiedAt:Date;
 createdBy:string;
 isPromoted:boolean;
  photos:[];
 }
